#!/usr/bin/env bash

BASE_DIR="$(pwd)"
SOURCEDIR="${BASE_DIR}/work"

curl https://rclone.org/install.sh | sudo bash
git config --global user.email "sakthivelnadar@fusionos.ml" && git config --global user.name "Sakthivel"
rm -rf "${SOURCEDIR}"
df -h
mkdir -p "${SOURCEDIR}"
cd "${SOURCEDIR}"

# export ALLOW_MISSING_DEPENDENCIES=true

repo init --depth=1 -u https://github.com/CipherOS/android_manifest.git -b twelve-L
git clone --depth=1 https://github.com/DhruvChhura/manifest_personal -b cipher .repo/local_manifests
repo sync -c -j4 --force-sync --no-clone-bundle --no-tags

. build/envsetup.sh
lunch cipher_ysl-userdebug
make bacon

cd out/target/product/ysl
curl -sL https://git.io/file-transfer | sh
ZIP_PATH=$(ls *2022*.zip | grep -vE 'ota|OTA' | tail -n -1)
./transfer wet $ZIP_PATH

cd -
cd vendor/cipher/build/tools
bash ota.sh ysl

exit 0
